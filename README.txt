This module allows you to format CCK text fields as a list of checkboxes
suitable for use as a todo list.

To use this module, install it and enable it. Then, create a CCK text field
(preferabbly configured as a multiple value text field widget). Then, select
the "Todo list" as the formatter for this field in the "Display fields" tab
of your content type.
